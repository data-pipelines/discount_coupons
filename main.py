from datetime import datetime, timedelta
from airflow import DAG
from airflow.models import Variable
from airflow.utils.dates import days_ago
from airflow.operators.bash import BashOperator
from airflow.exceptions import AirflowException


def main():
    pipeline_configuration = {
        'owner': 'clients_pipeline',
        'depends_on_past': False,
        'start_date': datetime(2023, 3, 25),
        'email_on_failure': False,
        'email_on_retry': False,
        'retries': 3,
        'retry_delay': timedelta(minutes=5)
    }

    unify_birthdays = DAG(
        'birthday_coupons',
        default_args=pipeline_configuration,
        description='Retrieve birthday dates, format and unify them',
        schedule_interval=timedelta(days=1)
    )

    run_hello_world = BashOperator(
        task_id='hello_world',
        bash_command='echo "Hello, World!"',
        dag=unify_birthdays
    )

    run_hello_world


if __name__ == '__main__':
    main()
